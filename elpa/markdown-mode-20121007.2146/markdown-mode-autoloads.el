;;; markdown-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (gfm-mode markdown-mode markdown-cleanup-list-numbers)
;;;;;;  "markdown-mode" "markdown-mode.el" (20604 20082 195265 21000))
;;; Generated autoloads from markdown-mode.el

(autoload 'markdown-cleanup-list-numbers "markdown-mode" "\
Update the numbering of numbered markdown lists

\(fn)" t nil)

(autoload 'markdown-mode "markdown-mode" "\
Major mode for editing Markdown files.

\(fn)" t nil)

(autoload 'gfm-mode "markdown-mode" "\
Major mode for editing GitHub Flavored Markdown files.

\(fn)" t nil)

;;;***

;;;### (autoloads nil nil ("markdown-mode-pkg.el") (20604 20082 344303
;;;;;;  563000))

;;;***

(provide 'markdown-mode-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; markdown-mode-autoloads.el ends here
