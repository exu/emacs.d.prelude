(set-default-font "-unknown-DejaVu Sans Mono-normal-normal-normal-*-15-*-*-*-m-0-iso10646-1")

;;(disable-theme 'zenburn)
;;(enable-theme 'monokai)
;;(load-theme 'solarized-dark t)

(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(ansi-color-for-comint-mode-on)

;; Variables
(setq fringe-mode (cons 4 0)
      echo-keystrokes 0.1
      font-lock-maximum-decoration t
      inhibit-startup-message t
      transient-mark-mode t
      color-theme-is-global t
      shift-select-mode nil
      mouse-yank-at-point t
      require-final-newline nil
      truncate-partial-width-windows nil
      uniquify-buffer-name-style 'forward
      ffap-machine-p-known 'reject

      fill-column 120
      whitespace-line-column 120
      whitespace-style (quote (trailing tabs newline tab-mark newline-mark))
      whitespace-display-mappings
      '(
        (space-mark 32 [183] [46]) ; normal space, ·
        (space-mark 160 [164] [95])
        (space-mark 2208 [2212] [95])
        (space-mark 2336 [2340] [95])
        (space-mark 3616 [3620] [95])
        (space-mark 3872 [3876] [95])
        (newline-mark 10 [172 10]) ; newlne, ¶¬
        (tab-mark 9 [9655 9] [92 9]) ; tab, ▷
        )


      ediff-window-setup-function 'ediff-setup-windows-plain
      oddmuse-directory (concat dotfiles-dir "oddmuse")
      xterm-mouse-mode t
      save-place-file (concat dotfiles-dir "places")
      scroll-margin 0
      scroll-conservatively 0
      scroll-preserve-screen-position 1
      default-directory "~/Workspace"

      tags-table-list '("/home/exu/Workspace/crm")

      make-backup-files nil
      auto-save-default nil

      shift-select-mode 1

      dired-listing-switches "-lXGh --group-directories-first"
      tramp-default-method "scpc"

      ;; IDO mode
      ido-enable-prefix nil
      ido-enable-flex-matching t
      ido-create-new-buffer 'always
      ido-use-filename-at-point 'guess
      ido-max-prospects 10

      default-truncate-lines t ;; disable line wrap
      truncate-partial-width-windows nil ;; make side by side buffers function the same as the main window
      longlines-auto-wrap nil

      initial-major-mode 'text-mode ; change scratch buffer to text-mode
      initial-scratch-message ""    ; change scratch buffer message
      linum-format "%4d "           ; number format in line number
      abbrev-file-name "~/.emacs.d/abbrevs"

      mouse-wheel-scroll-amount '(3)
      mouse-wheel-progressive-speed nil

      org-agenda-window-setup 'current-window
      compare-ignore-whitespace 1
      )

(set-fill-column 120) ; fill column didn't work

;; Indenting
(setq-default indent-tabs-mode nil
              tab-width 4
              )

(setq c-basic-offset 4
      yaml-indent-offset 4
      js-indent-level 4
      )

(add-to-list 'recentf-exclude "ido.hist")
(add-to-list 'recentf-exclude ".ido.last")
(add-to-list 'recentf-exclude "/bookmarks")
(add-to-list 'recentf-exclude "/TAGS")



;;;; no shell EMACS

(when window-system
  (setq frame-title-format '(buffer-file-name "%f" ("%b")))
  (tooltip-mode -1)
  (mouse-wheel-mode t)
  (blink-cursor-mode -1)
  (add-hook 'before-make-frame-hook 'turn-off-tool-bar))


;;;; HOOKS

(add-hook 'html-mode-hook
          (lambda ()
            (interactive)
            ;; Default indentation is usually 2 spaces, changing to 4.
            (set (make-local-variable 'sgml-basic-offset) 4)
            (setq fill-column 120)
            ))

(add-hook 'sgml-mode-hook
          (lambda ()
            (interactive)
            (set (make-local-variable 'sgml-basic-offset) 4)
            (setq fill-column 120)
            ))

(add-hook 'log-edit-mode-hook
          (lambda () (interactive)
            (local-set-key (kbd "RET") 'log-edit-done)
            ))


(add-hook 'php-mode-hook (lambda()
                         (interactive)
                         (flymake-mode 1)
                         ))

(add-hook 'c-mode-hook (lambda()
                         (interactive)
                         (setq indent-tabs-mode nil)
                         ))

(add-hook 'term-mode-hook (lambda()
                         (interactive)
                         (linum-mode -1)
                         (message "Hello from hook")
                         ))



(add-hook 'sgml-mode-hook 'zencoding-mode)

(remove-hook 'text-mode-hook 'prelude-turn-on-flyspell)

(defun disable-guru-mode ()
  (guru-mode -1)
  )

(add-hook 'prelude-prog-mode-hook 'disable-guru-mode t)




;;;  defun prelude-prog-mode-hook ()
;;;  "Default coding hook, useful with any programming language."
;;;  ;(flyspell-prog-mode)
;;;  (prelude-local-comment-auto-fill)
;;;  (prelude-turn-on-whitespace)
;;;  (prelude-turn-on-abbrev)
;;;  (prelude-add-watchwords)
;;;  (linum-mode +1)
;;;  ;; keep the whitespace decent all the time
;;;  (add-hook 'before-save-hook 'whitespace-cleanup nil t))

(defun fix-prelude-prog-mode-defaults ()
  (turn-off-flyspell))

(add-hook 'prelude-prog-mode-hook 'fix-prelude-prog-mode-defaults t)


(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)

;;;; END HOOKS



;; MODES
;;(require 'linum)
(require 'dired-single)

;; (smex-initialize) ;; better M-x

(recentf-mode 1)       ; Save a list of recent files visited.
(show-paren-mode 1)    ; Highlight matching parentheses when the point is on them.
(ido-mode t)
(global-linum-mode 1)
(global-hl-line-mode -1)
(delete-selection-mode 1) ; emacs doesn't delete selected text
;;(global-smart-tab-mode 1) ; does'nt work well
(whitespace-mode 1)
(winner-mode 1) ;winner-undo winner-redo for windows management
(menu-bar-mode 1)

(flyspell-mode nil)
;(ruby-block-mode nil)
(scroll-bar-mode -1)
(abbrev-mode 1)
(paredit-mode -1)
(auto-fill-mode nil)
(guru-mode -1)


(require 'iy-go-to-char)


;; Develop in ~/emacs.d/mysnippets, but also
;; try out snippets in ~/Downloads/interesting-snippets
(setq yas/root-directory '("~/.emacs.d/snippets"
                           "~/.emacs.d/elpa/yasnippet-20120923.1126/snippets"
                           ))
;; Map `yas/load-directory' to every element
(mapc 'yas/load-directory yas/root-directory)



;;;   ;; Multiple marking watch in http://emacsrocks.com/e08.html
;;;   (require 'inline-string-rectangle)
;;;   (global-set-key (kbd "C-x r t") 'inline-string-rectangle)
;;;
;;;   (require 'mark-more-like-this)
;;;   (global-set-key (kbd "C-<") 'mark-previous-like-this)
;;;   (global-set-key (kbd "C->") 'mark-next-like-this)
;;;   (global-set-key (kbd "C-M-m") 'mark-more-like-this) ; like the other two, but takes an argument (negative is previous)
;;;


;;(require 'rename-sgml-tag)
;;(define-key sgml-mode-map (kbd "C-c C-r") 'rename-sgml-tag)
;;
;;(require 'js2-rename-var)
;;(define-key js2-mode-map (kbd "C-c C-r") 'js2-rename-var)

;; https://github.com/winterTTr/ace-jump-mode
;;;(require 'ace-jump-mode)
;;;(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)
;;;(define-key global-map (kbd "`") 'ace-jump-mode)

;; https://github.com/magnars/expand-region.el
(require 'expand-region)
(global-set-key (kbd "C-@") 'er/expand-region)
(global-set-key (kbd "C-#") 'er/contract-region)

(require 'gpicker)

;; org export generic - i've copied from git repo (no in emacs
;; snapshot?)
(require 'org-generic "org-export-generic")

(require 'workgroups)
(setq wg-prefix-key (kbd "C-c w"))
(workgroups-mode 1)
(setq wg-morph-on nil)

(message dotfiles-dir)

(require 'psvn)
(setq svn-status-hide-unmodified 1)
(add-to-list 'svn-status-elided-list "config/databases.yml")
(add-hook 'svn-status-mode-hook (lambda()
                         (interactive)
                         (add-to-list 'svn-status-elided-list "config/databases.yml")
                         ))


(require 'gist)
(setq gist-use-curl nil)
;;(setq gist-view-gist nil)


(require 'zencoding-mode)

;;; (require 'rvm)
;;; (rvm-use-default)






(require 'multiple-cursors)
(global-set-key (kbd "s-SPC") 'set-rectangular-region-anchor)

;; From active region to multiple cursors:
(global-set-key (kbd "C-─") 'mc/edit-lines)
(global-set-key (kbd "C-·") 'mc/edit-ends-of-lines)
(global-set-key (kbd "C-µ") 'mc/edit-beginnings-of-lines)


;(global-set-key (kbd C-"M-æ") 'mc/mark-all-like-this)
(global-set-key (kbd "C-←") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-→") 'mc/mark-next-like-this)
(global-set-key (kbd "C-↓") 'mc/mark-more-like-this-extended)
(global-set-key (kbd "C-¶") 'mc/mark-all-in-region)





;;; -------------------------------------------------------------------------
;;; LAST LINES
;;; -------------------------------------------------------------------------
(setq machine-based-file (concat dotfiles-dir "machine/" system-name ".el"))
(when (file-exists-p machine-based-file)
  (message (concat "Loading: " machine-based-file))
  (load machine-based-file)
  )

(add-hook 'after-init-hook (lambda () (setq debug-on-error nil)))
