;;(require 'auto-complete-config)
;;(add-to-list 'ac-dictionary-directories "~/.emacs.d/ac-dict")

;; (add-to-list 'load-path "~/.emacs.d/custom/autocomplete")
;;

(require 'auto-complete-config)

;; Add ac-source-dictionary to ac-sources of all buffer
(defun ac-common-setup ()
  (setq ac-sources (append ac-sources '(ac-source-gtags))))


(ac-config-default)
;;(setq ac-auto-start 2)
;;(setq ac-delay 0.1)
;;(setq ac-auto-show-menu nil)
;;(setq ac-show-menu-immediately-on-auto-complete t)
;;(setq ac-trigger-key nil)


(define-key ac-mode-map (kbd "A-]") 'auto-complete)


;; (require 'auto-complete-yasnippet) ;; hack dont work
;;(require 'auto-complete-python)
;;(require 'auto-complete-css)
;;(require 'auto-complete-cpp)
;;(require 'auto-complete-emacs-lisp)
;;(require 'auto-complete-semantic)
;;(require 'auto-complete-gtags)

(ac-config-default)
(setq ac-auto-start 2)
(setq ac-delay 0.1)
(setq ac-auto-show-menu nil)
(setq ac-show-menu-immediately-on-auto-complete t)
(setq ac-trigger-key nil)
(setq ac-dwim t)

(global-auto-complete-mode t)

(set-default 'ac-sources '(ac-source-yasnippet ac-source-abbrev ac-source-words-in-buffer ac-source-files-in-current-dir ac-source-symbols))
