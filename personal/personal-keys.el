(put 'paredit-forward-barf-sexp 'disabled  "Fuck you!\n")
(put 'paredit-backward-barf-sexp 'disabled "Fuck you!\n")

(global-set-key [C-right] 'next-buffer)
(global-set-key [C-left] 'previous-buffer)

(global-set-key [C-tab] 'yas/expand)
(global-set-key (kbd "<C-tab>") 'yas/expand)

(global-set-key (kbd "<f1>") 'org-agenda-list)
(global-set-key (kbd "<C-f1>") (lambda () (interactive) (org-todo-list 1) ))
(global-set-key (kbd "<C-S-f1>") 'org-open-index-file)
(global-set-key (kbd "<C-M-f1>") 'org-open-daily-file)
(global-set-key (kbd "<s-f1>") 'open-scratch-buffer)

(global-set-key (kbd "<f2>") 'save-buffer)
(global-set-key (kbd "<C-f2>") 'save-some-buffers)

(global-set-key (kbd "<f3>") 'helm)
(global-set-key (kbd "<C-f3>") 'follow-delete-other-windows-and-split)

(global-set-key (kbd "<f4>") ( lambda () (interactive) ( dired ".")))
(global-set-key (kbd "<C-f4>") 'list-bookmarks)
(global-set-key (kbd "<C-S-f4>") 'toggle-truncate-lines)
(global-set-key (kbd "<M-C-f4>") (lambda () (interactive) (save-buffers-kill-emacs t) ))

(global-set-key (kbd "<f5>") 'flymake-mode)

(global-set-key (kbd "<C-f5>") 'flymake-goto-next-error)
(global-set-key (kbd "<C-S-f5>") 'flymake-goto-prev-error)

(global-set-key (kbd "<f6>") 'flyspell-mode)
(global-set-key (kbd "<C-f6>") 'cycle-ispell-languages)

(global-set-key (kbd "<f7>") 'rgrep)
;;(global-set-key (kbd "<f7>") 'ack)
(global-set-key (kbd "<C-f7>") 'find-name-dired)

(global-set-key (kbd "<f8>") 'next-error)
(global-set-key (kbd "<C-f8>") 'previous-error)

(global-set-key (kbd "<f9>") (lambda () (interactive) (erc :server "irc.freenode.net" :port 6667 :nick "ex00") ) )
(global-set-key (kbd "<C-f9>") 'twit)
(global-set-key "\C-c\C-t" 'twittering-update-status-interactive)

(global-set-key (kbd "<f11>") (lambda ()
                                (interactive)
                                (save-buffer)
                                (vc-next-action nil)
                              ))

(global-set-key (kbd "<C-f11>") (lambda ()
                                  (interactive)
                                  (svn-status)
                                  (svn-status-update (dired))
                                  ))

(global-set-key (kbd "<C-f12>") 'open-scratch-buffer)
(global-set-key (kbd "<C-S-f12>") 'open-recent-sql-file)

(global-set-key "\M-." 'etags-select-find-tag-at-point)
(global-set-key "\M-?" 'etags-select-find-tag)

;;;;; replaced by helm.
;; (global-set-key (kbd "M-x") 'smex)
;; (global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; ;; This is your old M-x.
;; (global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)


(global-set-key (kbd "M-m") 'iy-go-to-char)
(global-set-key (kbd "M-M") 'iy-go-to-char-backward)
(global-set-key (kbd "C-c F") 'iy-go-to-char-backward)
(global-set-key (kbd "C-c ;") 'iy-go-to-char-continue)
(global-set-key (kbd "C-c ,") 'iy-go-to-char-continue-backward)


(global-set-key (kbd "C-M-]") 'complete-tag)
(global-set-key (kbd "M-[") 'auto-complete)

(define-key ac-mode-map (kbd "M-[") 'auto-complete)



; Make Emacs use "newline-and-indent" when you hit the Enter key so
; that you don't need to keep using TAB to align yourself when coding.
(global-set-key "\C-m"          'newline-and-indent)
(global-set-key (kbd "C-`")     'prelude-ido-goto-symbol)
(global-set-key (kbd "M-p")     'gpicker-find-file)
(global-set-key (kbd "C-c C-v") (lambda () (interactive) (gpicker-visit-project default-directory)))

(global-set-key (kbd "M-;") 'comment-dwim-line)

(global-set-key (kbd "M-9") 'selective-display-toggle) ;;;personal-functions.el
(global-set-key (kbd "C-c 1") 'wg-switch-to-index-1)
(global-set-key (kbd "C-c 2") 'wg-switch-to-index-2)
(global-set-key (kbd "C-c 3") 'wg-switch-to-index-3)
(global-set-key (kbd "C-c 4") 'wg-switch-to-index-4)
(global-set-key (kbd "C-c 5") 'wg-switch-to-index-5)
(global-set-key (kbd "C-c 6") 'wg-switch-to-index-6)
(global-set-key (kbd "C-c 0") 'wg-switch-to-index-0)

(define-key dired-mode-map (kbd "<backspace>") (lambda nil (interactive) (joc-dired-single-buffer "..")))
