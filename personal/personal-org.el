;; org-mode
(setq org-directory "~/org/")
(setq org-default-notes-file "~/Dropbox/Dokumenty/org/notes.org")
(define-key global-map "\C-cc" 'org-capture)
(setq org-return-follows-link 1)
(setq org-log-done t)

(setq
 org-agenda-files (quote ("~/org/projects/index.org" "~/org/euro2012.org" "~/org/work/crm/todo.org" "~/org/work/network.org" "~/org/projects/sajter.org" "~/Dropbox/Dokumenty/org/english.org" "~/org/work/todo.org" "~/org/priv/todo.org" "~/org/work/crm/deploy.org"))
 org-link-frame-setup (quote ((vm . vm-visit-folder) (gnus . gnus) (file . find-file) (wl . wl)))
 org-startup-folded nil)


(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-document-title ((t (:foreground "#259185" :height 1.6))))
 '(org-link ((t (:foreground "yellow" :underline t))))
 '(org-table ((t (:foreground "deep sky blue")))))


(setq org-ditaa-jar-path "/usr/share/ditaa/ditaa.jar")


(setq org-todo-keywords
      '((sequence "TODO"
                  "PENDING"
                  ;;"FUTURE"
                  "DONE"
                  )))

(setq org-todo-keyword-faces
      '(
        ("FUTURE" . (:foreground "lightblue" :weight bold))
        ("PENDING"  . (:foreground "orange" :weight bold))
        ))




(setq org-capture-templates
      '(
        ("p" "Private Todo" entry (file+headline "~/org/priv/todo.org" "Private todo's")
         "* TODO %?\n  %i\n  %a")
        ("w" "Work Todo" entry (file+headline "~/org/work/todo.org" "Work todo's")
         "* TODO %?\n  %i\n  %a")
        ("c" "CRM Todo" entry (file+headline "~/org/work/crm/todo.org" "CRM todo's")
         "* TODO %?\n  %i\n  %a")
        ("r" "Recrutations" entry (file+headline "~/org/work/recrutation.org" "Recrutation todo's")
         "* TODO %?\n  %i\n  %a")
        ("j" "Journal" entry (file+datetree "~/org/journal.org")
         "* %?\nEntered on %U\n  %i\n  %a")
        ("e" "English" entry (file+datetree "~/org/english.org")
         "* %?\nEntered on %U\n  %i\n  %a")
        ))


;; active Babel languages
(org-babel-do-load-languages
 'org-babel-load-languages
 '((ruby . t)
   (ditaa . t)
   (python . t)
   (emacs-lisp . nil)
   (sh . nil)
   ))


(setq org-agenda-custom-commands
      '(("w" todo "TODO"
         ((org-agenda-sorting-strategy '(priority-down))
          (org-agenda-prefix-format "  Mixed: ")))
        ))
